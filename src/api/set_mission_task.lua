local logger = CinCLogger
local Group = Group

local command = function(arguments)
  logger.info('Command set_mission_task called')

  local group = Group.getByName(arguments['group_name'])
  local controller = group:getController()
 
  logger.info('Setting task')
  controller:setTask(arguments['task'])

  local response = {}
  return response
end

CinCServer.add_command('set_mission_task', command)