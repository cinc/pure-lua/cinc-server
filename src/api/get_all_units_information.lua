local logger = CinCLogger
local coalition = coalition
local coord = coord
local ipairs = ipairs
local table = table

CachedUnitInformation = {}

local function get_data(groups)

  -- The Cinc Java FX map uses Google Maps to get Lat/Long. It looks like it doesn't quite match up to
  -- what we see in DCS. So add some offsets to reduce the delta. These were fiddled with manually by
  -- testing unit spawning accuracy @ Lat/Long 42.24062040154821, 42.03635413606048} (Senaki Airfield
  -- west end of runway on the edge where it meets dirt path going south in Satellite view
  local X_OFFSET = 185
  local Y_OFFSET = 208 

  local data = {}
  
  for _, group in ipairs(groups) do
    for _, unit in ipairs(group:getUnits()) do    
      local unit_information = {}
      
      local unitPosition = unit:getPosition()
      
      local lat, lon, alt = coord.LOtoLL(unitPosition.p)
      
      unit_information['id'] = unit:getID()
      unit_information['name'] = unit:getName()
      unit_information['latitude'] = lat
      unit_information['longitude'] = lon
      unit_information['altitude'] = alt
      unit_information['callsign'] = unit:getCallsign()
      unit_information['type'] = unit:getTypeName()     
      table.insert(data, unit_information)
    end    
  end
  
  return data

end

local command = function (arguments)
  logger.info('get_all_units_information')
   
  CachedUnitInformation['blue'] = get_data(coalition.getGroups(coalition.side.BLUE))
  CachedUnitInformation['red'] = get_data(coalition.getGroups(coalition.side.RED))
  
  return CachedUnitInformation
end

CinCServer.add_command('get_all_units_information', command)