local logger = CinCLogger
local group_option_category = {}
local group = group
local AI = AI

group_option_category[0] = "Air"
group_option_category[2] = "Ground"
group_option_category[3] = "Naval"

-- Example JSON
-- {
--   "name": "set_rules_of_engagement",
--   "arguments": {
--     "group_name": "EXAMPLE_GROUP",
--     "roe": "OPEN_FIRE"
--   }
-- }
-- For a list of ROE states and their text names see https://wiki.hoggitworld.com/view/DCS_option_roe
local command = function(arguments)
  logger.info('Command set_rules_of_engagement called')

  local group = Group.getByName(arguments['group_name'])
  local category_id = group:getCategory()
  local controller = group:getController()

  local roe_id = AI['Option'][group_option_category[category_id]]['id']['ROE']
  local roe_value = AI['Option'][group_option_category[category_id]]['val']['ROE'][arguments['roe']]
  
  controller:setOption(roe_id, roe_value)

  local response = {}
  return response
end

CinCServer.add_command('set_rules_of_engagement', command)