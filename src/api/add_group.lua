local logger = CinCLogger
local coalition = coalition
local coord = coord
local JSON = loadfile("Scripts\\JSON.lua")()
local ipairs = ipairs

local command = function (arguments)
  logger.info('Command add_group called')

  -- Convert the unit starting position's Lat/Long to DCS's internal coordinate system
  for i, v in ipairs(arguments['group_data']['units']) do
    local unit_vec3 = coord.LLtoLO(v['x'], v['y']) 
    v['x'] = unit_vec3['x']
    v['y'] = unit_vec3['z']
  end

  -- Convert the Lat/Long of the group's waypoints
  for i, v in ipairs(arguments['group_data']['route']['points']) do
    local waypoint_vec3 = coord.LLtoLO(v['x'], v['y'])
    v['x'] = waypoint_vec3['x']
    v['y'] = waypoint_vec3['z']
  end
   
  -- coaltion.addGroup is supposed to return the created group. However in testing it does not return properly when
  -- creating an air unit in my experience (unit has an ID of 0). I may be doing something wrong here. 
  -- local group = coalition.addGroup(arguments['country_id'], arguments['group_category'], arguments['group_data'] )
  -- However, creating the group THEN getting it by name appears to work...
  coalition.addGroup(arguments['country_id'], arguments['group_category'], arguments['group_data'])
  local group = Group.getByName(arguments['group_data']['name'])

  if group and group['id_'] > 0 then
    local response = {}
    response['group_name'] = group:getName()
    response['group_id'] = group:getID()
    response['group_category'] = group:getCategory()
    return response  
  else
    error('Group not created successfully')
  end  
end

CinCServer.add_command('add_group', command)