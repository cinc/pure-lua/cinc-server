--[[
  Full credit for this code goes to FlightControl's MOOSE framework.
  This code allows fully dynamic loading which also allows remote debugging
--]]

env.info('*** CINC INCLUDE START ***')

local base = _G

__CinC = {}

__CinC.Include = function( IncludeFile )
  if not __CinC.Includes[ IncludeFile ] then
    __CinC.Includes[IncludeFile] = IncludeFile
    local f = assert( base.loadfile( IncludeFile ) )
    if f == nil then
      error ("CINC: Could not load CINC file " .. IncludeFile )
    else
      env.info( "CINC: " .. IncludeFile .. " dynamically loaded." )
      return f()
    end
  end
end

__CinC.Includes = {}
__CinC.Include( 'Scripts/cinc/src/logger.lua' )
__CinC.Include( 'Scripts/cinc/src/server.lua' )
__CinC.Include( 'Scripts/cinc/src/api/add_group.lua' )
__CinC.Include( 'Scripts/cinc/src/api/get_airbases.lua' )
__CinC.Include( 'Scripts/cinc/src/api/get_countries.lua' )
__CinC.Include( 'Scripts/cinc/src/api/get_country_toe.lua' )
__CinC.Include( 'Scripts/cinc/src/api/set_alarm_state.lua' )
__CinC.Include( 'Scripts/cinc/src/api/set_rules_of_engagement.lua' )

env.info('*** CINC INCLUDE END ***')